package com.item.demo.item;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/test/resources/features/bag.feature" , plugin = {"pretty", "html:target/cucumber  "})
public class CucumberTest
{

}
