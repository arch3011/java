package com.item.demo.item;

import com.item.demo.item.controller.ItemsController;
import com.item.demo.item.model.Items;
import com.item.demo.item.service.ItemsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class mockitoTest
{
    @Mock
    ItemsService itemsService;

    @InjectMocks
    ItemsController itemsController;

    @Test
    public void getMockito()
    {
        List<Items> list = new ArrayList<Items>();
        Items i1 = new Items((long)1, "John", 11, 11);
        Items i2 = new Items((long)2, "Alex", 23, 12);
        Items i3 = new Items((long)3, "Steve", 46, 13);
        list.add(i1);
        list.add(i2);
        list.add(i3);
        when(itemsService.getAllItems()).thenReturn(list);
        ResponseEntity<List<Items>> itemsList = itemsController.getItems();
        assertEquals(HttpStatus.OK, itemsList.getStatusCode());

//          String jsn = "{\"name\":\"abc\",\"quantity\":2,\"price\":1111}";
//        ResponseEntity<List<Items>> fil = itemsController.getItems();
//          when(itemsService.getAllItems()).thenReturn(ResponseEntity<List<Items>> {})
//          assertEquals();
    }

    @Test
    public void createMockito()
    {
        Items items = new Items((long)1,"ri",34,11);
        when(itemsService.createItems(items)).thenReturn(HttpStatus.OK);
        HttpStatus itemsResponse = itemsController.createItems(items);
        assertEquals(HttpStatus.OK, itemsResponse);
     //   verify(itemsService, times(1)).createItems(items);
    }

    @Test
    public void updateMockito()
    {
        Items items = new Items((long)1,"ri",34,11);
        long i = 2;
        ResponseEntity<Items> itemResp = itemsController.updateItems(i,items);
        when(itemsService.updateItemms(i,items)).thenReturn(items);
        assertEquals(HttpStatus.OK,itemResp.getStatusCode());
    }

    @Test
    public void deleteMockito()
    {
        long i= 1;
        HttpStatus status = itemsController.deleteItemsById(i);
        when(itemsService.deleteItems(i)).thenReturn(HttpStatus.OK);
        assertEquals(HttpStatus.OK,status);
    }
}