#Scenario: Get item record
#  Given I Set GET item service api endpoint
#  When I Set request HEADER
#  Then Send GET HTTP request
#  And I receive valid HTTP response code 200
#
#  Scenario: Add item record
#    Given I Set POST item service api endpoint
#    When I Set request HEADER
#    Then Send POST HTTP request
#    And I receive valid response

Feature: GetOperation Verify different GET operations using REST-assured

  Scenario: Verify one author of the post
    Given I perform GET operations for "http://localhost:8082/4"
    When perform get "http://localhost:8082/4"
    Then Response Status should be "abc"


  Scenario:
      Given check data exist for "http://localhost:8082/items/21"
      When Update for existing object
      Then Response Body name should be "ABC"

#
#    Scenario: Add Employee record
#      Given I perform POST operation for "http://localhost:8082/item/create"
#      When No data exist
##      And Send a POST HTTP request
#      Then Response Status should be "200"
#
#
#

  Scenario: Post data
      Given check data for "http://localhost:8082/items"
      When create new object for "http://localhost:8082/items"
      Then Response Body id should be "12"