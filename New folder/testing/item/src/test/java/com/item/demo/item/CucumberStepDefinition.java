package com.item.demo.item;

import com.item.demo.item.model.Items;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CucumberStepDefinition{


    int statusCode;
    public String url;

    Items items = new Items();
    Items createNote = new Items();

    int a;
    RestTemplate restTemplate = new RestTemplate();

    //get testing

    @Given("^I perform GET operations for \"([^\"]*)\"$")
    public void iPerformGETOperationsFor(String url) throws Throwable {
        System.out.println("URL: " + url);
        ResponseEntity<Items> response = restTemplate.getForEntity(url,Items.class);
        statusCode = response.getStatusCodeValue();
        System.out.println(statusCode);
    }

    @When("^perform get \"([^\"]*)\"$")
    public Items performGet(String arg0) throws Throwable {
        this.url = url;
        ResponseEntity<Items> response = restTemplate.getForEntity(url, Items.class);
        items = response.getBody();
        return items;
    }

    @Then("^Response Status should be \"([^\"]*)\"$")
    public void responseStatusShouldBe(String arg0) throws Throwable {
        Assert.assertEquals(Integer.parseInt(arg0),this.statusCode);
    }



    //update

    Items finalNote = new Items();

    @Given("^check data exist for \"([^\"]*)\"$")
        public Items performPutOperationFor(String url){
           this.url = url;
           ResponseEntity<Items> response = restTemplate.getForEntity(url, Items.class);
           items = response.getBody();
           return items;
        }

        @When("^Update for existing object$")
        public void updateForExistingObject(){
            Items updatedItem = new Items();
            updatedItem.setName("ABC");
            updatedItem.setPrice(2000);
            if(items != null){
               restTemplate.put(this.url, updatedItem);
            }
        }
    @Then("^Response Body name should be \"([^\"]*)\"$")
    public void responseBodyNameShouldBe(String title) {
        finalNote = performPutOperationFor(this.url);
        Assert.assertEquals(title,finalNote.getName());
    }



//create

    @Given("^check data for \"([^\"]*)\"$")
    public void checkDataFor(String arg0) throws Throwable {
        this.url = arg0;
        ResponseEntity<Items> response = restTemplate.getForEntity(url, Items.class);
        items = response.getBody();
    }


    @When("^create new object for \"([^\"]*)\"$")
    public void createNewObjectFor(Long arg0) throws Throwable {
        Items notes = new Items( 12L,"Abc", 21,3);
        ResponseEntity<Items> result = restTemplate.postForEntity(url, notes, Items.class);
        a = result.getStatusCodeValue();
        createNote =  result.getBody();
    }

    @Then("^Response Body id should be \"([^\"]*)\"$")
    public void responseBodyIdShouldBe(Long id) throws Throwable {
        Assert.assertEquals(createNote.getId(),Long.valueOf(id));
    }

}