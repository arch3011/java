package com.item.demo.service;


import com.item.demo.model.Items;
import com.item.demo.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemsService {
    @Autowired
    ItemsRepository itemsRepository;

    //
//    public List<Items> getAllItems() {
//        List<Items> result = (List<Items>) itemsRepository.findAll();
//        if(result.size() > 0) {
//            return result;
//        } else {
//            return new ArrayList<Items>();
//        }
//    }
    public List<Items> getAllItems() {
        return itemsRepository.findAll();
    }

//    public List<EmployeeEntity> getAllEmployees()
//    {
//        List<EmployeeEntity> result = (List<EmployeeEntity>) repository.findAll();
//
//        if(result.size() > 0) {
//            return result;
//        } else {
//            return new ArrayList<EmployeeEntity>();
//        }
//    }


//    public Items getItemById(Long id) throws ResourceNotFoundException {
//        Items items = itemsRepository.findById(id).get();
//        return items;
//    }
    public Items getItemById(Long id) {
        Optional<Items> items = itemsRepository.findById(id);
        return items.get();
    }
//    public HttpStatus createItems(Items items) {
//
//        if (items.getId() == null) {
//            items = itemsRepository.save(items);
//
//            //  return entity;
//        }
//        return HttpStatus.OK;
//    }
    //______________
    public Items createItems(Items items) {
        if (items.getId() == null) {
            itemsRepository.save(items);
            return items;
        } else {
            Optional<Items> employee = itemsRepository.findById(items.getId());

            if (employee.isPresent()) {
                Items newEntity = employee.get();
                newEntity.setName(items.getName());
                newEntity.setPrice(items.getPrice());
                newEntity.setQuantity(items.getQuantity());

                newEntity = itemsRepository.save(newEntity);

                return newEntity;
            } else {
                items = itemsRepository.save(items);

                return items;
            }
        }
    }
    //___________________________
//    public Items updateItemms(Long id, Items item) {
//        Items items = itemsRepository.findById(id).get();
//        items.setPrice(item.getPrice());
//        items.setName(item.getName());
//        Items updatedItems = itemsRepository.save(items);
//        return updatedItems;
//    }

//    public HttpStatus deleteItems(Long id) throws ResourceNotFoundException {
//        Items items = itemsRepository.findById(id).get();
//        if (items != null) {
//            itemsRepository.delete(items);
//            return HttpStatus.OK;
//        } else {
//            throw new ResourceNotFoundException("Items", "id", id);
//        }
//    }

    //_______________
    public void deleteItems(Long id)
    {
        Optional<Items> employee = itemsRepository.findById(id);
            itemsRepository.deleteById(id);
    }
    //_______________
}
