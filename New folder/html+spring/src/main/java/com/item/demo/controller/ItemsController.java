package com.item.demo.controller;


import com.item.demo.exception.ResourceNotFoundException;
import com.item.demo.model.Items;
import com.item.demo.repository.ItemsRepository;
import com.item.demo.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;



@Controller
public class ItemsController {
    @Autowired
    ItemsService itemsService;
    ItemsRepository itemsRepository;
//    @RequestMapping("/")
//    public String getHome(){
//        return "index";
//    }

//    @GetMapping("/item")
//    public String getItems(Model model) {
//        List<Items> list = itemsService.getAllItems();
//        model.addAttribute("list",list);
//        return "getItems";
//    }
    @GetMapping("/get")
    public ResponseEntity<List<Items>> getItems(Items items) {
        List<Items> list = itemsService.getAllItems();
        //model.addAttribute("list",list);
        return new ResponseEntity<List<Items>>(list , HttpStatus.OK);
    }

//        @GetMapping("/item")
//    public String getItems(Model model) {
//        List<Items> list = itemsService.getAllItems();
//        model.addAttribute("list",list);
//        return "getItems";
//        //new ModelAndView("getItems","list",list)
//    }

    @GetMapping("/item/{id}")
    public String itemsById(Model model, @PathVariable("id") Long id) throws ResourceNotFoundException {

        Items entity = itemsService.getItemById(id);
        model.addAttribute("item", entity);
        return "index";
        //Items items = itemsService.getItemById(id);
        //return new ResponseEntity<Items>(items, HttpStatus.OK);
    }


    @PostMapping(value = "/cre")
    public ResponseEntity<Items> createItems(Items items) throws ResourceNotFoundException {
        items =itemsService.createItems(items);
       // model.addAttribute("item", itemsService.createItems(items));
        return  new ResponseEntity<Items>(items,HttpStatus.OK);
        //return "createItem";

    }
//
//    @PutMapping("/item/create/{id}")
//    public ResponseEntity<Items> updateItems(@PathVariable(value = "id") Long id, @RequestBody Items items)
//            throws ResourceNotFoundException {
//        Items updated = itemsService.updateItemms(id, items);
//        return new ResponseEntity<Items>(updated, HttpStatus.OK);
//    }

    @DeleteMapping("/item/{id}")
    public HttpStatus deleteItemsById(@PathVariable("id") Long id)
            throws ResourceNotFoundException {
        itemsService.deleteItems(id);
        return HttpStatus.OK;
    }


    @GetMapping("/signup")
    public String showSignUpForm(Items items) {
        return "createItem";
    }

    @GetMapping("/item/create")
    public String showUpdateForm() {
        //model.addAttribute("items", itemsRepository.findAll());
        return "createItem";
    }
}