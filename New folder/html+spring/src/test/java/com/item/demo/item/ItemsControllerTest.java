package com.item.demo.item;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

     MockMvc mockMvc;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void getItem() throws Exception{
        MvcResult mvcResult = mockMvc.perform(get("/item")).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200,status);
    }

    @Test
    public void addItem1() throws Exception{

        String jsn="{\"name\":\"abc\",\"company\":\"cons\",\"price\":2500}";
        MvcResult mvcResult = mockMvc.perform(post("/item/create")
                .accept(MediaType.APPLICATION_JSON).content(jsn)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
                int status = mvcResult.getResponse().getStatus();

        Assert.assertEquals(200,status);
    }

    @Test
    public void updateItemTest() throws Exception {
        String jsn = "{\"name\":\"abc\",\"quantity\":2,\"price\":1111}";
        MvcResult mvcResult = mockMvc.perform(put("/item/create")
                .accept(MediaType.APPLICATION_JSON).content(jsn)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();

        Assert.assertEquals(200,status);
    }
    @Test
    public void deleteItemTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/item/3")).andReturn();
        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals(200,status);
    }
}
