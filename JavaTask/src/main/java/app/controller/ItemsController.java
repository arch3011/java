package app.controller;

import app.exception.ResourceNotFoundException;
import app.repository.ItemsRepository;
import app.model.Items;
import app.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.rmi.runtime.Log;

import java.util.List;

@RestController
@RequestMapping("/item")
public class ItemsController
{
    @Autowired
    ItemsService itemsService;

    @GetMapping
    public ResponseEntity<List<Items>> getItems()
    {
    List<Items> list = itemsService.getAllItems();
        return new ResponseEntity<List<Items>>(list, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Items> itemsById(@PathVariable("id") Long id) throws ResourceNotFoundException
    {
    Items items = itemsService.getItemById(id);
    return new ResponseEntity<Items>(items, HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public void createItems(@RequestBody Items items) throws ResourceNotFoundException
    {
        itemsService.createItems(items);
    }

    @PutMapping("/create/{id}")
    public ResponseEntity<Items> updateItems(@PathVariable(value = "id") Long id ,@RequestBody Items items)
            throws ResourceNotFoundException {
        Items updated = itemsService.updateItemms(id, items);
        return new ResponseEntity<Items>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteMobileById(@PathVariable("id") Long id)
            throws ResourceNotFoundException {
        itemsService.deleteItems(id);
        return HttpStatus.OK;
    }


}
