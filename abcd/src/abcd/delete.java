package abcd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
public class delete {
	public static void main(String[] args) {
		try{
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mobile", "root", "consultadd"); 
			PreparedStatement ps = con.prepareStatement("DELETE from emp WHERE id = 114");
			
			int st=ps.executeUpdate();
			if (st > 0)
                System.out.println("Successfully Deleted"+st);
            else
                System.out.println("Deletion Failed");
            con.close();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
	}
}