package drive;
import org.aspectj.lang.annotation.DeclareAnnotation;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Controller;

@org.springframework.boot.autoconfigure.SpringBootApplication
@Controller
public class SpringBootApplication {
	public static void main(String args[])
	{
	SpringApplication.run(SpringBootApplication.class, args);
}
	}
