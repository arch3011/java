package drive;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import drive.mobileentity;

@Repository
public interface mobilerepository extends JpaRepository<mobileentity, Long> {

}
