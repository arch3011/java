package drive;

import java.util.ArrayList;
import java.util.List;
import drive.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 
import drive.mobileentity;
import drive.mobilerepository;
 
@Service
public class mobileservice 
{
	
	 @Autowired
	 mobilerepository repository;
	  
	 public List<mobileentity> getAllMobile()
	    {
	        List<mobileentity> employeeList = repository.findAll();
	         
	        if(employeeList.size() > 0) {
	            return employeeList;
	        } else {
	            return new ArrayList<mobileentity>();
	        }
	    }

	    public mobileentity getMobileById(Long id) throws RecordNotFoundException 
	    {
	        mobileentity employee = repository.findById(id).get();

	            return employee;
	        
	    }
	     
	    public mobileentity createOrUpdateMobile(mobileentity entity)
	    {
	       
	         mobileentity mobile = new mobileentity();
	         System.out.println("ENtity: " + entity);
	        
	        	mobile.setId(entity.getId());
	        	mobile.setName(entity.getName());
	        	mobile.setCompany(entity.getCompany());
	        	mobile.setPrice(entity.getPrice());
	 
	        	return repository.save(mobile);
	        
	    } 
	    
	   
	    public void deleteMobileById(Long id) throws RecordNotFoundException 
	    {
	        mobileentity mobile = repository.findById(id).get();
	         
	        if(mobile!=null)
	        {
	            repository.deleteById(id);
	        } else {
	            throw new RecordNotFoundException("No employee record exist for given id");
	        }
	    } 
	}
