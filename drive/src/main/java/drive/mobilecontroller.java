package drive;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import drive.RecordNotFoundException;


@RestController
@RequestMapping("/mobile")
public class mobilecontroller 
{

    @RequestMapping("/q")
    public String getHome(){
        return "index";
    }

    @Autowired
    mobileservice service;
 
    @GetMapping
    public ResponseEntity<List<mobileentity>> getAllMobile() {
        List<mobileentity> list = service.getAllMobile();
 
        return new ResponseEntity<List<mobileentity>>(list, new HttpHeaders(), HttpStatus.OK);
    }
 
    @GetMapping("/{id}")
    public ResponseEntity<mobileentity> getMobileById(@PathVariable("id") Long id) 
                                                    throws RecordNotFoundException {
        mobileentity entity = service.getMobileById(id);
 
        return new ResponseEntity<mobileentity>(entity, new HttpHeaders(), HttpStatus.OK);
    }
 
    @PostMapping
    public ResponseEntity<mobileentity> createOrUpdateMobile(@RequestBody mobileentity employee)
                                                    throws RecordNotFoundException {
        mobileentity updated = service.createOrUpdateMobile(employee);
        return new ResponseEntity<mobileentity>(updated, new HttpHeaders(), HttpStatus.OK);
    }
 
    @DeleteMapping("/{id}")
    public HttpStatus deleteMobileById(@PathVariable("id") Long id) 
                                                    throws RecordNotFoundException {
        service.deleteMobileById(id);
        return HttpStatus.FORBIDDEN;
    }
	
}
